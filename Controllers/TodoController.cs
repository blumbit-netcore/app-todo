﻿using App_Todo.Data;
using App_Todo.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App_Todo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        //Usamos DBContext para crear los métodos del CRUD

        private readonly TodoDbContext _todoDbContext;

        // Creamos un constructor
        public TodoController(TodoDbContext todoDbContext)
        {
            _todoDbContext = todoDbContext;
        }

        // Creacion de los metodos del CRUD
        // Obtener todos los registros
        [HttpGet]
        public async Task<IActionResult> GetAllTodos()
        { 
            var todos = await _todoDbContext.Todos.Where(x => x.IsDeleted == false).OrderByDescending(x => x.CreatedDate).ToListAsync();

            return Ok(todos);
        }
        // Crear un registro
        [HttpPost]
        public async Task<IActionResult> AddTodo(Todo todo)
        { 
            // Adiciono el registro
            _todoDbContext.Todos.Add(todo);
            // Guardar el registro
            await _todoDbContext.SaveChangesAsync();

            return Ok(todo);
        }
        //Actualizar un registro
        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> UpdateTodo([FromRoute] Guid id, Todo todoUpdateRequest)
        {
            // Obtener el registro a actualizar
            var todo = await _todoDbContext.Todos.FindAsync(id);

            // Verifico si el registro tiene datos
            if (todo == null) return NotFound(); //error 404 Not Found

            // Actualizo el registro
            todo.Description = todoUpdateRequest.Description;
            todo.IsCompleted = todoUpdateRequest.IsCompleted;
            todo.DeletedDate = todoUpdateRequest.DeletedDate;
            todo.CompletedDate = DateTime.Now;

            //Guardo los cambios
            await _todoDbContext.SaveChangesAsync();

            return Accepted(todo);
        }
        //Eliminar un registro
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> deleteTodo([FromRoute] Guid id)
        {
            // Obtener el registro a eliminar
            var todo = await _todoDbContext.Todos.FindAsync(id);

            // Verifico si el registro tiene datos
            if (todo == null) return NotFound(); //error 404 Not Found

            // Cambiamos el valor del flag IsDeleted
            todo.IsDeleted = true;
            todo.DeletedDate = DateTime.Now;

            // Guardamos los cambios
            await _todoDbContext.SaveChangesAsync();

            return Ok(todo);
        }
    }
}
