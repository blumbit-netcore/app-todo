﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App_Todo.Model
{
    public class Todo
    {
        // Propiedades de la clase
        // Anotaciones que ayudan a modelar de forma especifica al atributo en la BBDD
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string? Description { get; set; }

        [Required]
        public DateTime? CreatedDate { get; set;}

        public bool IsCompleted { get; set; } = false;

        public DateTime? CompletedDate { get; set;}

        public bool IsDeleted { get; set; } = false;

        public DateTime? DeletedDate { get; set;}
    }
}
