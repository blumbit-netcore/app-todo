﻿using App_Todo.Model;
using Microsoft.EntityFrameworkCore;

namespace App_Todo.Data
{
    public class TodoDbContext : DbContext
    {
        // Constructor
        public TodoDbContext(DbContextOptions options) : base(options) 
        { 
            // Aqui se escribe código para realizar otras operaciones
            // Como solamente estamos realizando la creación de una tabla en la BBDD
            // esto se deja en blanco
        }

        // Realizamos la operación de Generar la Tabla a partir de la Clase
        public DbSet<Todo> Todos { get; set; }
        
    }
}
